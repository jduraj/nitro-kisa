<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package nitro
 */
?>

	</div><!-- #content -->

<?php if (!is_home() || !is_front_page()) { ?>
	<?php get_sidebar('footer'); ?>
<?php  } ?>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info container">
			<?php echo ( get_theme_mod('nitro_footer_text') == '' ) ? ('&copy; '.date('Y').' '.get_bloginfo('name').__('. All Rights Reserved. ','nitro')) : esc_html( get_theme_mod('nitro_footer_text') ); ?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
	
</div><!-- #page -->


<?php wp_footer(); ?>

</body>
</html>
